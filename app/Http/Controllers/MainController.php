<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UserImport;
use App\Imports\ProductImport;

class MainController extends Controller
{
    public function first(Request $request)
    {
    	$user = User::where('codigo', $request->code)->first();
    	if ($user != null) {
    		return view('first')->with(["user" => $user]);	
    	}
    	else
    	{
    		return view('welcome')->with(["msg" => "Error en el código"]);
    	}
    	
    }

    public function list()
    {
    	return view('list');
    }

    public function uploadView()
    {
    	return view('upload');
    }

    public function upload(Request $request)
    {
    	$file = $request->file('file');
    	Excel::import(new UserImport, $file);
    }

    public function uploadProducts(Request $request)
    {
    	$file = $request->file('file');
    	Excel::import(new ProductImport, $file);
    }
}
