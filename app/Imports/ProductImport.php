<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Product;

class ProductImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $total_rows = count($collection);

        for ($i=1; $i < $total_rows; $i++)
        {
            if (is_numeric($collection[$i][0]) && is_numeric($collection[$i][1])) {
                $product = new Product;
                $product->codigo_venta = $collection[$i][0];
                $product->descripcion = $collection[$i][2];
                $product->precio = $collection[$i][3];
                $product->puntaje = $collection[$i][4];
                $product->save();
            }
            elseif(is_numeric($collection[$i][0]))
            {
                $product->descripcion = $product->descripcion. "<br>". $collection[$i][2];
                $product->save();
            }
            else
            {
                continue;
            }
        	
        }
    }
}
