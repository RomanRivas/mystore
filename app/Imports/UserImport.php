<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\User;

class UserImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $total_rows = count($collection);

        for ($i=2; $i < $total_rows; $i++)
        {
        	$user = new User;
        	$user->name = $collection[$i][3];
        	$user->codigo = $collection[$i][2];
        	$user->email_gn = $collection[$i][10];
        	$user->save();
        }
    }
}
