<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'MAYTA FRIAS YOVANNA GEORGINA',
            'email_gn' => 'admin@admin.com',
            'codigo' => '2422088',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
