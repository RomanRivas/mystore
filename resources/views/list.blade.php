<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Natura</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">

    </head>
    <body >
        <div class="flex-center position-ref full-height" style="background-image: url('images/fondo.png');">
            <div class="content">
                <div class="title m-b-md">
                    <div class="row ml-5 mt-5">
                        <div class="col-12 table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Cantidad</th>
                                        <th>Descripcion</th>
                                        <th>Precio</th>
                                        <th>Puntos</th>
                                        <th>Total Puntos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Codigo</td>
                                        <td>Codigo</td>
                                        <td>Codigo</td>
                                        <td>Codigo</td>
                                        <td>Codigo</td>
                                        <td>Codigo</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="{{asset('js/app.js')}}"></script>


<script>
    $(document).ready(function() {
        ajax()
    });
</script>

</html>
