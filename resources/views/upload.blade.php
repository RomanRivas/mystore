<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Natura</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">

        
    </head>
    <body >
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="m-5 p-3">
                    <form method="post" action="../upload" enctype="multipart/form-data">
                        @csrf
                        <label>Importar Usuarios</label>
                        <input type="file" name="file" id="file">
                        <button>Enviar</button>
                    </form>
                </div>

                <div class="m-5 p-3">
                    <form method="post" action="../upload/products" enctype="multipart/form-data">
                        @csrf
                        <label>Importar Productos</label>
                        <input type="file" name="file" id="file">
                        <button>Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script src="{{asset('js/app.js')}}"></script>
</html>
